// Load a library to cache data in the browser
import { get, set } from 'idb-keyval';

// import library to queue concurrent requests.
import { Mutex } from 'async-mutex';

// Load the electrum library.
import { ElectrumClient, ElectrumTransport } from 'electrum-cash';

// Set up a local variable to hold a reference to an electrum client.
let electrum: ElectrumClient;

// Set up a queue manager.
const getClientLock = new Mutex();

export const getElectrumClient = async function(): Promise<any>
{
	// Set up mutex to queue concurrent requests.
	const unlock = await getClientLock.acquire();

	try
	{
		if(!electrum)
		{
			// Initialize an electrum client.
			electrum = new ElectrumClient('Merchant Dashboard', '1.4.3', 'electrum.imaginary.cash', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);

			// Wait for the client to connect
			await electrum.connect();
		}

		return electrum;
	}
	finally
	{
		unlock();
	}
}

export const getCurrentBlockHeight = async function(): Promise<number>
{
	// Use the local electrum connection.
	const client = await getElectrumClient();

	// Request the current block height.
	const { height } = await client.request('blockchain.headers.subscribe');

	// Return the current block height.
	return Number(height);
}

export const getAddressBalance = async function(address: string): Promise<number>
{
	// Use the local electrum connection.
	const client = await getElectrumClient();

	// Request the current address balance.
	const { confirmed, unconfirmed } = await client.request('blockchain.address.get_balance', address);

	// Return the current address balance.
	return Number(confirmed) + Number(unconfirmed);
}

export const getAddressHistory = async function(address: string): Promise<any>
{
	// Use the local electrum connection.
	const client = await getElectrumClient();

	// Request the address history.
	const history = await client.request('blockchain.address.get_history', address);

	// Return the address history.
	return history;
}

export const getTransaction = async function(transactionHash: string): Promise<any>
{
	const cachedTransaction = await get(`transaction[${transactionHash}]`);

	if(cachedTransaction)
	{
		return cachedTransaction;
	}

	// Use the local electrum connection.
	const client = await getElectrumClient();

	// Request the address history.
	const transactionHex = await client.request('blockchain.transaction.get', transactionHash, false);

	// Cache the transaction hex.
	await set (`transaction[${transactionHash}]`, transactionHex);

	// Return the transaction hex.
	return transactionHex;
}
