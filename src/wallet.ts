// Import types
import type
{
	BlockHeight,
	Satoshis,
	PublicKey,
	ExtendedPublicKey,
	Address,
	AddressHistory,
	ExtendedAddressHistory,
	TransactionHash,
	TransactionData,
	TransactionList,
	DerivationIndex,
	DerivationType,
} from './interfaces.d';

// Import dependencies.
import { hexToBin, binToHex, decodeCashAddress, decodeTransactionUnsafe, lockingBytecodeToCashAddress } from '@bitauth/libauth';
import { deriveAddressFromExtendedPublicKey } from './hdkeys';
import { reduceNumberListToSum } from './utils';
import { getAddressBalance, getAddressHistory, getTransaction, getCurrentBlockHeight } from './network';

// Import constants.
import { BLOCKS_PER_DAY, SATOSHIS_PER_BITCOIN_CASH } from './constants';

// Set up constants to handle wallet behaviour.
const EXTENDED_DERIVATION_GAP_LENGTH = 20;

const RECEIVING_ADDRESS = 0;
const CHANGE_ADDRESS = 1;

export class ReadOnlyWallet
{
	public walletName: string;

	// Store the wallet entroy (the data that we make a wallet from)
	private walletEntropy: PublicKey | ExtendedPublicKey | Address;

	// Store a list of addresses related to this wallet.
	public walletAddresses: Set<Address> = new Set();

	// Store a list of transactions related to this wallet.
	public walletTransactions: TransactionList = new Map();

	// Store balance numbers on a per-addres basis.
	public walletBalancePerAddress: Map<Address, Satoshis> = new Map();

	// Set up variable to track extended public key management.
	private extendedAddressHistory: ExtendedAddressHistory =
	{
		// Receiving address history.
		0: {},

		// change address history.
		1: {},
	}

	// Set up variable to track gap management.
	private extendedDerivationGaps: Record<number, number> =
	{
		// Receiving address gap.
		0: 0,

		// change address gap.
		1: 0,
	}

	// Set up a variable to track current block height.
	private currentBlockHeight: BlockHeight = 0;

	// Create a getter function that sums up the total unspent satoshis in the wallet.
	public get balanceInSatoshis(): number
	{
		return Array.from(this.walletBalancePerAddress.values()).reduce(reduceNumberListToSum, 0);
	}

	private getTransactionsSinceBlockHeight(blockHeight: BlockHeight): Array<number>
	{
		let transactions = [];

		for(const { height, tx_hash, received } of this.walletTransactions.values())
		{
			if(height >= blockHeight)
			{
				transactions.push(received);
			}
		}

		return transactions;
	}

	// Create a set of getter functions to return transactions from various periods of time.
	public get transactionsAllTime(): Array<number>
	{
		return this.getTransactionsSinceBlockHeight(0);
	}

	public get transactionsLastDay(): Array<number>
	{
		return this.getTransactionsSinceBlockHeight(this.currentBlockHeight - (BLOCKS_PER_DAY * 1));
	}

	public get transactionsLastWeek(): Array<number>
	{
		return this.getTransactionsSinceBlockHeight(this.currentBlockHeight - (BLOCKS_PER_DAY * 7));
	}

	public get transactionsLastMonth(): Array<number>
	{
		return this.getTransactionsSinceBlockHeight(this.currentBlockHeight - (BLOCKS_PER_DAY * 30));
	}

	public get transactionsLastQuarter(): Array<number>
	{
		return this.getTransactionsSinceBlockHeight(this.currentBlockHeight - (BLOCKS_PER_DAY * 90));
	}

	// Create a set of getter functions to return recived satoshis from various periods of time.
	public get receivedSatoshisAllTime(): number
	{
		return this.getTransactionsSinceBlockHeight(0).reduce(reduceNumberListToSum, 0);
	}

	public get receivedSatoshisLastDay(): number
	{
		return this.getTransactionsSinceBlockHeight(this.currentBlockHeight - (BLOCKS_PER_DAY * 1)).reduce(reduceNumberListToSum, 0);
	}

	public get receivedSatoshisLastWeek(): number
	{
		return this.getTransactionsSinceBlockHeight(this.currentBlockHeight - (BLOCKS_PER_DAY * 7)).reduce(reduceNumberListToSum, 0);
	}

	public get receivedSatoshisLastMonth(): number
	{
		return this.getTransactionsSinceBlockHeight(this.currentBlockHeight - (BLOCKS_PER_DAY * 30)).reduce(reduceNumberListToSum, 0);
	}

	public get receivedSatoshisLastQuarter(): number
	{
		return this.getTransactionsSinceBlockHeight(this.currentBlockHeight - (BLOCKS_PER_DAY * 90)).reduce(reduceNumberListToSum, 0);
	}

	public get statusHighBalance(): boolean
	{
		// Return low balance indication if current balance is between 1 satoshi and 0.1 BCH.
		return (this.balanceInSatoshis > (10 * SATOSHIS_PER_BITCOIN_CASH) ? true : false);
	}

	public get statusLowBalance(): boolean
	{
		if(this.balanceInSatoshis === 0)
		{
			return false;
		}

		// Return low balance indication if current balance is between 1 satoshi and 0.1 BCH.
		return (this.balanceInSatoshis < (0.1 * SATOSHIS_PER_BITCOIN_CASH) ? true : false);
	}

	public get statusNoVolume(): boolean
	{
		const volumeLastMonth = this.getTransactionsSinceBlockHeight(this.currentBlockHeight - (BLOCKS_PER_DAY * 30)).reduce(reduceNumberListToSum, 0);

		// Return low volume indication if volume per quarter is less than 1 BCH.
		return (volumeLastMonth === 0 ? true : false);
	}

	public get statusLowVolume(): boolean
	{
		// Return false if there is no volume, to prevent double warnings.
		if(this.statusNoVolume)
		{
			return false;
		}

		const volumeLastMonth = this.getTransactionsSinceBlockHeight(this.currentBlockHeight - (BLOCKS_PER_DAY * 30)).reduce(reduceNumberListToSum, 0);

		// Return low volume indication if volume per quarter is less than 1 BCH.
		return (volumeLastMonth <= (1 * SATOSHIS_PER_BITCOIN_CASH) ? true : false);
	}

	public get statusNoActivity(): boolean
	{
		const transactionsLastMonth = this.getTransactionsSinceBlockHeight(this.currentBlockHeight - (BLOCKS_PER_DAY * 30)).length;

		// Return no activity indication if transactions per month is 0.
		return (transactionsLastMonth <= 0 ? true : false);
	}

	public get statusLowActivity(): boolean
	{
		// Return false if there is no activity, to prevent double warnings.
		if(this.statusNoActivity)
		{
			return false;
		}

		const transactionsLastWeek = this.getTransactionsSinceBlockHeight(this.currentBlockHeight - (BLOCKS_PER_DAY * 7)).length;

		// Return low activity indication if transactions per week is less than 7.
		return (transactionsLastWeek < 1 ? true : false);
	}

	//
	constructor(source: PublicKey | ExtendedPublicKey | Address, name: string)
	{
		// Store this wallets name.
		this.walletName = name;

		// Store the source as this wallets entropy.
		this.walletEntropy = source;

		let prefix = '';
		if(!source.startsWith('bitcoincash:'))
		{
			prefix = 'bitcoincash:';
		}

		// If source is an address, add it to the wallet.
		const decodedResult = decodeCashAddress(prefix + source);
		if(typeof decodedResult !== 'string')
		{
			this.addAddressToWallet(prefix + source);
		}

		// TODO: If source is a public key, add a P2PKH address derived from it to the wallet.
		// NOTE: This is not important to implement today.

		// If source is an extended public key, add a list of P2PKH addresses derived from it to the wallet..
		if(source.startsWith('xpub'))
		{
			// .. by deriving the next set of addresses and letting the wallet continue to deriving until it detects a gap.
			this.deriveNextExtendedPublicKeyAddress(RECEIVING_ADDRESS);
			this.deriveNextExtendedPublicKeyAddress(CHANGE_ADDRESS);
		}

		// Update the current block height.
		this.updateCurrentBlockHeight();
	}

	private async updateCurrentBlockHeight(): Promise<void>
	{
		this.currentBlockHeight = await getCurrentBlockHeight();
	}

	private async deriveNextExtendedPublicKeyAddress(derivationType: DerivationType): Promise<void>
	{
		// Ignore this if we don't have a wallet that can derive keys.
		if(!this.walletEntropy.startsWith('xpub'))
		{
			return;
		}

		// Determine the index for the next address of the given type.
		const nextDerivationIndex = Object.keys(this.extendedAddressHistory[derivationType]).length;

		// Derive the next address for the given type.
		const nextAddress = await deriveAddressFromExtendedPublicKey(this.walletEntropy, nextDerivationIndex, derivationType);

		// Add the derived address to this wallet.
		this.addAddressToWallet(nextAddress, nextDerivationIndex, derivationType);
	}

	private async addAddressToWallet(address: Address, derivationIndex: DerivationIndex = 0, derivationType: DerivationType = 0): Promise<void>
	{
		// Add the address to the wallet.
		this.walletAddresses.add(address);

		// Fetch the balance of the address, then update local tracking of balance.
		this.getAddressBalance(address).then(this.handleUpdatedAddressBalance.bind(this, address));

		// Fetch the history of the address, then update local tracking of history.
		this.getAddressHistory(address).then(this.handleUpdatedAddressHistory.bind(this, address, derivationIndex, derivationType));
	}

	private async getAddressBalance(address: Address): Promise<Satoshis>
	{
		return getAddressBalance(address);
	}

	private async handleUpdatedAddressBalance(address: Address, balanceInSatoshis: Satoshis): Promise<void>
	{
		// Update the balance of this address.
		this.walletBalancePerAddress.set(address, balanceInSatoshis);
	}

	private async getAddressHistory(address: Address): Promise<AddressHistory>
	{
		return getAddressHistory(address);
	}

	private async handleUpdatedAddressHistory(address: Address, derivationIndex: DerivationIndex, derivationType: DerivationType, history: AddressHistory): Promise<void>
	{
		// Assume current address won't have history, and increase the gap.
		this.extendedDerivationGaps[derivationType] += 1;

		// Store the history for this address.
		this.extendedAddressHistory[derivationType][derivationIndex] = history;

		// If current address has history..
		if(history && history.length > 0)
		{
			// .. reset the derivation gap for this derivation type.
			this.extendedDerivationGaps[derivationType] = 0;

			// .. parse the history for this address in parallel.
			this.parseAddressHistory(address, derivationIndex, derivationType);
		}

		// Derive more keys if we have not yet passed the derivation gap.
		if(this.extendedDerivationGaps[derivationType] < EXTENDED_DERIVATION_GAP_LENGTH)
		{
			this.deriveNextExtendedPublicKeyAddress(derivationType);
		}
	}

	private async parseAddressHistory(address: Address, derivationIndex: DerivationIndex, derivationType: DerivationType): Promise<void>
	{
		const addressHistory = this.extendedAddressHistory[derivationType][derivationIndex];

		// For each transaction..
		for(const { tx_hash, height } of addressHistory)
		{
			// .. fetch the full transaction hex.
			const transactionHex = await getTransaction(tx_hash);

			// .. parse the transaction hex to get the outputs of the transaction.
			const { outputs } = decodeTransactionUnsafe(hexToBin(transactionHex));

			// .. assume the merchant has not received any funds.
			let received = 0;

			// .. then check each output..
			for(const { lockingBytecode, valueSatoshis } of outputs)
			{
				// .. and convert the locking bytecode for the sent funds to an address..
				const sentToAddress = lockingBytecodeToCashAddress(lockingBytecode, 'bitcoincash');

				// .. and increase the amount the merchant received if the address matches the merchants address.
				if(sentToAddress === address)
				{
					received += Number(valueSatoshis);
				}
			}

			// Store the satoshisSentToMerchant in the extended address history.
			this.addTransactionToWallet({ tx_hash, height, received });
		}
	}

	private async addTransactionToWallet(transaction: TransactionData): Promise<void>
	{
		this.walletTransactions.set(transaction.tx_hash, transaction);
	}
}
