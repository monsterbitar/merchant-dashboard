// Import interfaces.
import type { Merchant, MerchantMetrics, MerchantStatus, MerchantNotice } from './interfaces.d';

// Import utility to store data in the browser.
import { get, set, clear } from 'idb-keyval';

// Import constants
import { SATOSHIS_PER_BITCOIN_CASH } from './constants';

// Import utility functions.
import { sortByNumber, sortMerchantsByName, getMedianFromArray, getMaximumFromArray } from './utils';

// Import utilities to interact with the bitcoin cash blockchain.
import { getAddressBalance, getCurrentBlockHeight, getAddressHistory, getTransaction } from './network';

// Import utilities to ...
import { deriveAddressFromExtendedPublicKey } from './hdkeys';

// Import utilities to work with transactions, wallets and other bitcoin cash data structures.
import { hexToBin, decodeTransactionUnsafe, lockingBytecodeToCashAddress, binToBigIntUint64LE } from '@bitauth/libauth';

// import read only wallet class.
import { ReadOnlyWallet } from './wallet';

export class App
{
	public readOnly = false;

	// Set up a reference to currently selected files via user input.
	public selectedFiles;

	// TODO: Rename this to indicate that they relate to input fields.
	public merchantAddress: string;
	public merchantName: string;
	public merchantNode: number | undefined;

	public merchants: Map<string, Merchant>;
	public wallets: Map<string, ReadOnlyWallet> = new Map();

	public fiat: Record<string, number> = {};

	public selectedCurrency: string;
	public availableCurrencies: Map<string, string> = new Map();

	public exchangeRates: Map<string, number> = new Map();
	public exchangeRatesTimestamp: number;

	public get warnings(): Record<string, MerchantNotice>
	{
		// Initialize an empty list of warnings.
		const warnings: Record<string, MerchantNotice> = {};

		for(const [ address, wallet ] of this.wallets.entries())
		{
			// Show warning if there is no activity
			if(wallet.statusNoActivity)
			{
				warnings[address] = { icon: '⏹️', text: 'no activity last month' };
				continue;
			}

			// Show warning if there is low activity
			if(wallet.statusLowActivity)
			{
				warnings[address] = { icon: '⏸️', text: 'low activity last month' };
				continue;
			}

			// Show warning if there is low volume
			if(wallet.statusLowVolume)
			{
				warnings[address] = { icon: '📊', text: 'low volume last month' };
				continue;
			}

			// Show warning if there is low volume
			if(wallet.statusLowBalance)
			{
				warnings[address] = { icon: '👛', text: 'low balance' };
				continue;
			}

			// Show warning if there is low volume
			if(wallet.statusHighBalance)
			{
				warnings[address] = { icon: '💰', text: 'high balance' };
				continue;
			}
		}

		return warnings;
	}

	public get monthlyTransactions(): number
	{
		let transactions = 0;
		for(const wallet of Array.from(this.wallets.values()))
		{
			transactions += wallet.transactionsLastMonth.length;
		}
		return transactions;
	}

	public get monthlyVolume(): number
	{
		let volume = 0;
		for(const wallet of Array.from(this.wallets.values()))
		{
			volume += wallet.receivedSatoshisLastMonth;
		}
		return volume;
	}

	public get monthlyMaximum(): number
	{
		let maximum = 0;
		for(const wallet of Array.from(this.wallets.values()))
		{
			for(const received of wallet.transactionsLastMonth)
			{
				if(received > maximum)
				{
					maximum = received;
				}
			}
		}
		return maximum;
	}

	public get monthlyMedian(): number
	{
		let transactions = [];
		for(const wallet of Array.from(this.wallets.values()))
		{
			// Add the last months transactions from this wallet.
			transactions = [ ...transactions, ...wallet.transactionsLastMonth ];
		}

		return getMedianFromArray(transactions);
	}

	public get monthlyAverage(): number
	{
		let receivedCount = 0;
		let receivedTotal = 0;

		for(const wallet of Array.from(this.wallets.values()))
		{
			for(const received of wallet.transactionsLastMonth)
			{
				receivedCount += 1;
				receivedTotal += received;
			}
		}

		return (receivedCount ? receivedTotal / receivedCount : 0);
	}

	public get walletsWithSomeActivity(): number
	{
		let someActivityWallets = this.wallets.size;
		for(const wallet of Array.from(this.wallets.values()))
		{
			if(wallet.statusNoActivity || wallet.statusLowActivity)
			{
				someActivityWallets -= 1;
			}
		}

		return someActivityWallets;
	}

	public get walletsWithLowActivity(): number
	{
		let lowActivityWallets = 0;
		for(const wallet of Array.from(this.wallets.values()))
		{
			lowActivityWallets += (wallet.statusLowActivity ? 1 : 0);
		}

		return lowActivityWallets;
	}

	public get walletsWithNoActivity(): number
	{
		let noActivityWallets = 0;
		for(const wallet of Array.from(this.wallets.values()))
		{
			noActivityWallets += (wallet.statusNoActivity ? 1 : 0);
		}

		return noActivityWallets;
	}

	public get walletsWithSomeVolume(): number
	{
		let someVolumeWallets = this.wallets.size;
		for(const wallet of Array.from(this.wallets.values()))
		{
			if(wallet.statusNoVolume || wallet.statusLowVolume)
			{
				someVolumeWallets -= 1;
			}
		}

		return someVolumeWallets;
	}

	public get walletsWithLowVolume(): number
	{
		let lowVolumeWallets = 0;
		for(const wallet of Array.from(this.wallets.values()))
		{
			lowVolumeWallets += (wallet.statusLowVolume ? 1 : 0);
		}

		return lowVolumeWallets;
	}

	public get walletsWithNoVolume(): number
	{
		let noVolumeWallets = 0;
		for(const wallet of Array.from(this.wallets.values()))
		{
			noVolumeWallets += (wallet.statusNoVolume ? 1 : 0);
		}

		return noVolumeWallets;
	}

	public get walletsWithHighBalance(): number
	{
		let highBalanceWallets = 0;
		for(const wallet of Array.from(this.wallets.values()))
		{
			highBalanceWallets += (wallet.statusHighBalance ? 1 : 0);
		}

		return highBalanceWallets;
	}

	public get walletsWithLowBalance(): number
	{
		let lowBalanceWallets = 0;
		for(const wallet of Array.from(this.wallets.values()))
		{
			lowBalanceWallets += (wallet.statusLowBalance ? 1 : 0);
		}

		return lowBalanceWallets;
	}

	constructor(source: any)
	{
		this.initializeData();
	}

	async loadMerchantList(): Promise<void>
	{
		// Load the merchant list from local storage, or initialize an empty list if none could be found.
		this.merchants = await get('merchants') || new Map();

		// Sort the merchant list after loading it.
		await this.sortMerchantList();
	}

	async saveMerchantList(): Promise<void>
	{
		// Sort the merchant list before saving it.
		await this.sortMerchantList();

		// Store the merchant list to local storage, so we can load it when the user visits the page the next time.
		await set('merchants', this.merchants);
	}

	async sortMerchantList(): Promise<void>
	{
		// Convert the merchant list to an array, sort it by merchant name, then convert back to a Map and save it.
		this.merchants = new Map([ ...this.merchants ].sort(sortMerchantsByName));
	}

	// Load the currently selected currency, or default to US dollars.
	async loadSelectedCurrency(): Promise<void>
	{
		this.selectedCurrency = await get('selectedCurrency') || 'USD';
	}

	// Store the selected currency to local storage, so we can use it as the default on future visits.
	async saveSelectedCurrency(): Promise<void>
	{
		await set('selectedCurrency', this.selectedCurrency);
	}

	async initializeData(): Promise<void>
	{
		// Load the merchant list from local storage, or initialize an empty list if none could be found.
		await this.loadMerchantList();

		// Load the selected currency, or use the default if none could be found.
		await this.loadSelectedCurrency();

		// Fetch exchange rates, as those are needed in the next steps.
		await this.initializeExchangeRates();

		// Create wallet for all the merchants.
		for(const [ address, merchant ] of this.merchants)
		{
			this.wallets.set(address, new ReadOnlyWallet(address, merchant.name));
		}
	}

	async initializeExchangeRates()
	{
		// Initialize a small list of available currencies.
		this.availableCurrencies.set('AUD', 'Australian Dollars (AUD)');
		this.availableCurrencies.set('EUR', 'Euro (EUR)');
		this.availableCurrencies.set('USD', 'US Dollars (USD)');

		// Load exchange rate information from local storage cache.
		this.exchangeRates = await get('exchangeRates') || new Map();
		this.exchangeRatesTimestamp = await get('exchangeRateTimestamp') || 0;

		// Calculate the how old the cache is.
		const currentTimestamp = Math.round(Date.now() / 1000);
		const exchangeRateAge = currentTimestamp - this.exchangeRatesTimestamp;

		// Update cache if older than 5 minutes.
		if(exchangeRateAge > 60 * 5)
		{
			const vsCurrencies = Array.from(this.availableCurrencies.keys()).join(',');

			const response = await fetch(`https://api.coingecko.com/api/v3/simple/price?ids=bitcoin-cash&vs_currencies=${vsCurrencies}`);
			const priceData = await response.json();

			for(const index in priceData['bitcoin-cash'])
			{
				this.exchangeRates.set(index.toUpperCase(), priceData['bitcoin-cash'][index]);
			}

			// Store the updated rates in cache.
			await set('exchangeRates', this.exchangeRates);
			await set('exchangeRateTimestamp', currentTimestamp);
		}
	}

	async addOrUpdateMerchant(address: string, name: string, node: number = 0)
	{
		if(this.readOnly)
		{
			return;
		}

		if(!this.merchantAddress || !this.merchantName)
		{
			return false;
		}

		const newOrUpdatedMerchant: Merchant =
		{
			address: this.merchantAddress,
			name: this.merchantName,
		};

		// Update our internal state.
		this.merchants.set(this.merchantAddress, newOrUpdatedMerchant);

		// Store updated state in local storage.
		await this.saveMerchantList();

		// TODO: Process this merchants history.
		this.wallets.set(this.merchantAddress, new ReadOnlyWallet(this.merchantAddress, this.merchantName));

		// clear input fields.
		this.merchantAddress = '';
		this.merchantName = '';
	}

	async removeMerchant(address: string)
	{
		if(this.readOnly)
		{
			return;
		}

		// Update our internal state.
		this.merchants.delete(address);
		const changed = this.wallets.delete(address);

		// Store updated state in local storage, if it has changed.
		if(changed)
		{
			await this.saveMerchantList();
		}
	}

	async exportMerchantList(merchants: Record<string, Merchant>): Promise<void>
	{
		// Convert merchant data to a JSON string.
		const data = JSON.stringify(Object.fromEntries(this.merchants));

		// Create a blob data structure from the JSON string.
		const blob = new Blob([data], { type: 'application/json' });

		// Restructure the blob data into an object URL.
		const url = URL.createObjectURL(blob);

		// TODO: Set up a good name for the downloaded data.
		const filename = 'merchants.json';

		// Create an anchor element and attach the data and name above.
		const anchor = document.createElement('a');
		anchor.href = url;
		anchor.download = filename;

		// Add the anchor element to the document body..
		document.body.appendChild(anchor);

		// .. then fake a user clicking on it to generate a click event..
		anchor.click();

		// .. and finally remove the anchor from the body to clean up.
		document.body.removeChild(anchor);
	}

	async importMerchantList(): Promise<void>
	{
		// Find the file input element.
		const inputUploadFile = document.getElementById('inputUploadFile');

		// Simulate a user click
		inputUploadFile.click();
	}

	async parseImportedMerchantList(event): Promise<void>
	{
		try
		{
			// Parse the uploaded merchant list.
			const uploadedMerchantList = JSON.parse(event.target.result);

			// Replace the existing merchants with the uploaded merchant list.
			// @ts-ignore
			this.merchants = new Map(Object.entries(uploadedMerchantList));

			// Store the merchant list to local storage.
			await this.saveMerchantList();

			// Reload the page.
			location.reload();
		}
		catch(error)
		{
			throw(new Error(`Failed to parse merchant list: ${error}`));
		}
	}

	async handleUploadedMerchantList(): Promise<void>
	{
		try
		{
			// Create a new FileReader() object
			const fileReader = new FileReader();

			// Parse the merchant list after it has been read.
			fileReader.onload = this.parseImportedMerchantList.bind(this);

			// Read the first selected file.
			fileReader.readAsText(this.selectedFiles[0]);
		}
		catch (error)
		{
			throw(new Error(`Failed to upload merchant list: ${error}`));
		}
	}
}
