// Import dependencies
import naturalCompare from 'string-natural-compare';

// Import types
import type { Merchant } from './interfaces.d';

/**
 *
 */
export const reduceNumberListToSum = function(previousValue: number, currentValue: number)
{
	return previousValue + currentValue;
}

export const sortByNumber = function(a: number, b: number): number
{
	return (a - b);
}

export const sortMerchantsByName = function(a: [string, Merchant], b: [string, Merchant]): number
{
	const naturalResult = naturalCompare(a[1].name, b[1].name, {caseInsensitive: true})

	return naturalResult;
}


export const getMedianFromArray = function(values: Array<number>): number
{
	if(values.length === 0)
	{
		return 0;
	}

	values.sort(sortByNumber);

	let middleIndex = Math.floor(values.length / 2);

	if(values.length % 2)
	{
		return values[middleIndex];
	}

	return (values[middleIndex - 1] + values[middleIndex]) / 2.0;
}

export const getMaximumFromArray = function(values: Array<number>): number
{
	if(values.length === 0)
	{
		return 0;
	}

	values.sort(sortByNumber);

	return values[values.length - 1];
}
