// Import dependency functions.
import { binToHex, hash160, OpcodesBCH, encodeDataPush, lockingBytecodeToCashAddress, flattenBinArray, decodeHdPublicKey, deriveHdPath } from '@bitauth/libauth';

/**
 * Derives an address from an extended public key given a change and index value.
 *
 * @note this function assumes that the provided key is bip44 compatible and already derived to the account level.
 *       "m / purpose' / coin_type' / account' / change / address_index"
 */
export const deriveAddressFromExtendedPublicKey = async function(extendedPublicKey: string, index: number = 0, change: number = 0)
{
	// Attempt to decode the extended public key.
	const decodeResult = decodeHdPublicKey(extendedPublicKey);

	// Throw an error if we failed to decode.
	if(typeof decodeResult === 'string')
	{
		throw(new Error(decodeResult));
	};

	// Extract the node from the decoding result.
	const { node } = decodeResult;

	// Set up a derivation path matching the desired target.
	// NOTE: Using capital M denotes public key derivation.
	const derivationPath = `M/${change}/${index}`;

	// Attempt to derive the desired path.
	const derivationResult = deriveHdPath(node, derivationPath);

	// Throw an error if we failed to derive.
	if(typeof derivationResult === 'string')
	{
		throw(new Error(derivationResult));
	};

	// Extract the public key from the derivation result.
	const { publicKey } = derivationResult;

	// Convert the public key to a P2PKH address.
	const derivedAddress = await convertPublicKeyToCashAddress(publicKey)

	// Return the derived address.
	return derivedAddress;
}

export const convertPublicKeyToCashAddress =  async function(publicKey: Uint8Array): Promise<string>
{
	// Calculate a public key hash to use in a P2PKH locking script.
	const publicKeyHash = hash160(publicKey);

	// Assemble the P2PKH locking script from the op_code parts.
	const lockingScript =
	[
		Uint8Array.of(OpcodesBCH.OP_DUP),
		Uint8Array.of(OpcodesBCH.OP_HASH160),
		encodeDataPush(publicKeyHash),
		Uint8Array.of(OpcodesBCH.OP_EQUALVERIFY),
		Uint8Array.of(OpcodesBCH.OP_CHECKSIG),
	];

	// Construct an address by wrapping the public key into a P2PKH locking script.
	const address = lockingBytecodeToCashAddress(flattenBinArray(lockingScript), 'bitcoincash');

	// Throw an error if there was a problem with encoding the lockscript into a cash address.
	if(typeof address !== 'string')
	{
		throw(new Error(`Failed to create cash address from derived public key: ${binToHex(publicKey)}`));
	}

	// Return the derived address.
	return address;
}
